
import {
	IGame, 
	IRateStats, 
	IPlayersStats, 
	IStats,
} from './types/types';
import {SOURCE_TYPE} from './types/consts';

//OWN must be first to use AND at the end
const RATE_TYPES: SOURCE_TYPE[] = [SOURCE_TYPE.OWN, SOURCE_TYPE.TESERA, SOURCE_TYPE.BGG];

export function parseGames(games: IGame[]): IStats {
	const ownRatedIds = RATE_TYPES.reduce((acc, rateName) => {
		if (rateName === SOURCE_TYPE.OWN) {
			return acc;
		}

		acc[rateName] = {};
		
		return acc;
	}, {} as Record<SOURCE_TYPE, Record<number, boolean>>);
	const allRatesMap = RATE_TYPES.reduce((acc, rateName) => {
		acc[rateName] = {
			list: [],
			sum: 0,
		};

		if (rateName !== SOURCE_TYPE.OWN) {
			acc[SOURCE_TYPE.OWN + '_' + rateName] = {
				list: [],
				sum: 0,
				amounts: {},
			};
		}
		else {
			acc[rateName].amounts = {};
		}

		return acc;
	}, {} as Record<SOURCE_TYPE, {list: number[], sum: number, amounts?: Record<number, {value: number, amount: number}>}>);

	for (let i = 0; i < games.length; i++) {
		const {
			bggId,
			teseraId,
			rating,
		} = games[i];

		RATE_TYPES.forEach(rateName => {
			const value = rating[rateName];

			if (value !== void 0 && !Number.isNaN(value)) {
				const curRateStore = allRatesMap[rateName];
			
				curRateStore.sum += value;
				curRateStore.list.push(value);

				if (rateName === SOURCE_TYPE.OWN) {
					if (bggId) {
						ownRatedIds[SOURCE_TYPE.BGG][bggId] = true;
					}

					if (teseraId) {
						ownRatedIds[SOURCE_TYPE.TESERA][teseraId] = true;
					}

					if (!curRateStore.amounts![value]) {
						curRateStore.amounts![value] = {value, amount: 0};
					}

					curRateStore.amounts![value].amount++;
				}
				else if (
					(bggId && ownRatedIds[SOURCE_TYPE.BGG][bggId])
					|| (teseraId && ownRatedIds[SOURCE_TYPE.TESERA][teseraId])
				) {
					const ANDRateName = SOURCE_TYPE.OWN + '_' + rateName;
					const ANDRateStore = allRatesMap[ANDRateName];

					if (ANDRateStore) {
						ANDRateStore.sum += value;
						ANDRateStore.list.push(value);
					}
				}
			}
		});
	}

	const rateStats: IRateStats = {};
	const ratesUsed: SOURCE_TYPE[] = [];
	const ratingsList = Object.keys(allRatesMap);

	ratingsList.forEach(rateName => {
		const {
			list,
			sum,
			amounts,
		} = allRatesMap[rateName];
		const amount = list.length;

		if (amount) {
			list.sort((a, b) => a - b);

			const isOddAmount = amount % 2;
			const midIndex = Math.floor(amount / 2);
			let median = 0;

			if (isOddAmount) {
				median = list[midIndex];
			}
			else {
				median = (list[(midIndex - 1)] + list[midIndex]) / 2;
			}

			let modes: number[] = [];

			if (amounts) {
				let maxAmount = 0;
				const amountItems = Object.values(amounts) as {value: number, amount: number}[];
				
				amountItems.forEach((modeItem) => {
					const rateAmount = modeItem.amount;

					if (rateAmount > maxAmount) {
						modes = [];

						modes.push(modeItem.value);
						maxAmount = rateAmount;
					}
					else if (rateAmount === maxAmount) {
						modes.push(modeItem.value);	
					}
				});				
			}

			rateStats[rateName] = {
				amount,
				average: sum / amount,
				median,
			};

			if (modes.length) {
				rateStats[rateName].modes = modes;
			}

			ratesUsed.push(rateName as SOURCE_TYPE);
		}
	});

	return {
		amount: games.length,
		ratings: rateStats,
		ratingsList: ratesUsed,
	};
}