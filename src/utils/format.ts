
const formatter = new Intl.NumberFormat('en-US', {
   minimumFractionDigits: 0,      
   maximumFractionDigits: 2,
});

export function toFixed2Max(num: number) {
	return formatter.format(num);
}

export function capitalize(string: string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

export function formatSource(string: string) {
  if (string === 'bgg') {
    return 'BGG';
  }
  else {
    return capitalize(string);
  }
}