
export const enum COLLECTION_TYPE {
	RATED = 1,
	OWN = 2,
	PLAYED = 3,
}

export const enum SOURCE_TYPE {
	BGG = 'bgg',
	TESERA = 'tesera',
	OWN = 'own',
}