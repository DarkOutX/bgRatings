
import {SOURCE_TYPE} from './consts';

export interface IGame {
	bggId?: number;
	teseraId?: number;
	name: string;
	nameRu?: string;
	rating: {
		[SOURCE_TYPE.TESERA]?: number;
		[SOURCE_TYPE.BGG]?: number;
		[SOURCE_TYPE.OWN]?: number;
	}
	players?: {
		min: number;
		max: number;
	}
}

export interface IStat {
	average: number;
	median: number;
	amount: number;
	modes?: number[];
}

export type IRateStats = Partial<Record<SOURCE_TYPE, IStat>>;
export type IPlayersStats = Record<number, number>;

export interface IStats {
	amount: number;
	ratings: IRateStats;
	ratingsList: SOURCE_TYPE[];
	players?: IPlayersStats;
}