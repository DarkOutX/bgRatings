
import {COLLECTION_TYPE} from './types/consts';
import {IGame} from './types/types';

async function fetchJSON(url: string) {
	return fetch(url).then(res => {
		if (res.status === 500) {
			return {error: "Tesera API isn't working"};
		}
		
		return res.json();
	});
}

async function fetchText(url: string) {
	return fetch(url).then(res => res.text());
}
/*
const USER = {
	//ME: '563474',
	ME: 'DarkOutX',
	EVA: 'EVAluationEVA',
	DIMA: 'GFilin',
}
*/

interface ITeseraItem {
	game: {
		id?: number,
		teseraId: number,
		title: string;
		title2: string;
		ratingUser?: number;
		bggGeekRating?: number;
		ratingMy?: number;
	},
	creationDateUtc: string;
}

export async function fetchTesera(name: string, type: COLLECTION_TYPE): Promise<IGame[]> {
	let typeString: string;

	switch (type) {
		case COLLECTION_TYPE.RATED:
			typeString = 'top'; break;
		case COLLECTION_TYPE.OWN:
			typeString = 'own'; break;
		case COLLECTION_TYPE.PLAYED:
			typeString = 'played'; break;
	}

	const url = `https://api.tesera.ru/collections/base/${typeString}/${name}?limit=100`;
	let i = 0;
	let res = await fetchJSON(`${url}&offset=${i}`);
	const data: ITeseraItem[] = [];
	
	while (res.length || res.error) {
		if (res.error) {
			throw new Error(res.error);
		}
	
		data.push(...res);
		
		i++;
		
		res = await fetchJSON(`${url}&offset=${i}`);
	}
	
	return data.map((item) => {
		const {
			game: {
				teseraId,
				title: titleRu,
				title2: titleEn,
				ratingUser: tesera,
				bggGeekRating: bgg,
				ratingMy: my,
			},
			creationDateUtc: played,
		} = item;

		const game: IGame = {
			teseraId,
			name: titleEn || titleRu,
			nameRu: titleRu,
			rating: {
				own: my,
				bgg,
				tesera,
			},
		};

		return game;
	});
}

export async function fetchBGG(name: string, type: COLLECTION_TYPE): Promise<IGame[]> {
	let typeString: string;

	switch (type) {
		case COLLECTION_TYPE.RATED:
			typeString = 'rated'; break;
		case COLLECTION_TYPE.OWN:
			typeString = 'own'; break;
		case COLLECTION_TYPE.PLAYED:
			typeString = 'played'; break;
	}

	const url = `https://boardgamegeek.com/xmlapi2/collection?username=${name}&${typeString}=1&stats=1`;
	let data = await fetchText(url);
	let attempts = 5;

	//console.log(url);	
	//console.log(data);

	while ((!data || data.indexOf('Please try again later') >= 0) && attempts < 5) {
		await new Promise((res) => setTimeout(res, 1500));

		data = await fetchText(url);
		attempts++;
	}

	if (!data) {
		console.error('[fetchBGG] No data found');

		return [];
	}

	const xmlDOM = new window.DOMParser().parseFromString(data, "text/xml");

	return Array.from(xmlDOM.querySelectorAll('item')).map(item => {
		const bggId = parseInt(item.getAttribute('objectid') || '', 10);
		const name = item.querySelector('name')?.['innerHTML'] || '';
		const statsDOM = item.querySelector('stats')!;
		const minPlayers = parseInt(statsDOM.getAttribute('minplayers') || '', 10);
		const maxPlayers = parseInt(statsDOM.getAttribute('maxplayers') || '', 10);
		const ownRating = statsDOM.querySelector('rating')?.getAttribute('value');
		const rating = statsDOM.querySelector('rating average')?.getAttribute('value');

		const formedItem: IGame = {
			name,
			bggId,
			rating: {
    			own: Number(ownRating),
    			bgg: Number(rating),
			},
			players: {
				min: minPlayers,
				max: maxPlayers, 
			},
		};

		return formedItem;
	});
}