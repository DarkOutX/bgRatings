
import * as React from "react";
import {hot} from "react-hot-loader";
import {parseGames} from "../../utils/parse";
import {toFixed2Max, capitalize, formatSource} from "../../utils/format";
import {COLLECTION_TYPE, SOURCE_TYPE} from "../../utils/types/consts";
import type {IGame, IStats} from "../../utils/types/types";
import Hinted from "reactutils/src/components/Hinted/Hinted";
import Icon from "reactutils/src/components/Icon/Icon";
import './styles.scss';

const {useRef, useState} = React; 

interface IProps {
    games: IGame[];
}

export function Stats({games}: IProps) {
	const stats = parseGames(games);

    if (!stats) {
        return <div />;
    }

    const {
        amount,
        ratings,
        ratingsList = [],
    } = stats;

    const ownRatingDiffs: {
        formattedSource: string;
        diff: number;
    }[] = [];

    const DOMList = ratingsList
        .sort((a, b) => {
            if (a.indexOf('own') === 0) {
                return -1
            }
            else {
                return 1;
            }
        })
        .map((rateName, i) => {
            const rateItem = ratings[rateName]!;
            const {
                amount: amountForRating,
                average,
                median,
            } = rateItem;
            let formattedRateName: string = formatSource(rateName);
            let className = '';
            let hint = ``;
            
            if (rateName.indexOf('own_') === 0) {
                const source = rateName.split('_')[1];
                let formattedSource = formatSource(source);

                formattedRateName = `${formattedSource} (own)`;
                hint = `User's ratings from ${formattedSource} (only which you rated too)`;

                const ownAverage = ratings['own']?.average;
                
                if (ownAverage) {
                    ownRatingDiffs.push({
                        formattedSource,
                        diff: (ownAverage - average),
                    });
                }
            }
            else if (rateName.indexOf('own') === -1) {
                className = 'divided';
                hint = `User's ratings from ${formattedRateName}`;
            }
            else {
                hint = 'Your own ratings';
            }

            return <tr key={rateName} className={className}>
                <td>
                    <Hinted hint={hint}>
                        {formattedRateName}
                    </Hinted>
                </td>
                <td>{amountForRating}</td>
                <td>{toFixed2Max(average)}</td>
                <td>{toFixed2Max(median)}</td>
            </tr>
        })
    ;

    const ownModes = ratings[SOURCE_TYPE.OWN] ? ratings[SOURCE_TYPE.OWN]!.modes : void 0;

    return <div className='stats'>
        <p><b>Total: </b><span>{amount}</span></p>
        {amount ? 
            <table>
                <thead>
                    <tr>
                        <th></th>
                        <th><Hinted hint='Amount of rated games'>Amount</Hinted></th>
                        <th><Hinted hint='Average rating of games'>Average</Hinted></th>
                        <th><Hinted hint={`Median value is a value, when half games has higher rating and another half - lower`}>Median</Hinted></th>
                    </tr>
                </thead>
                <tbody>
                {DOMList}
                </tbody>
            </table>
            : void 0
        }
        {ownModes ? <p><b><Hinted hint='Your most popular rating'>Your modes</Hinted>:</b> {ownModes.join(', ')}</p>: void 0}
        {ownRatingDiffs.map(item => {
            const {
                formattedSource,
                diff,
            } = item;

            return <p key={formattedSource}>
                <b>{formattedSource} diff:</b> {diff.toFixed(2)} {(diff >= 0) ? <Icon name="happy" /> : <Icon name="evil" />}
            </p>
        })}
    </div>;
}

export default hot(module)(Stats);
