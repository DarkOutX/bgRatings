
import * as React from "react";
import {fetchBGG, fetchTesera} from "./utils/fetch";
import {parseGames} from "./utils/parse";
import {toFixed2Max} from "./utils/format";
import {hot} from "react-hot-loader";
import {COLLECTION_TYPE} from "./utils/types/consts";
import {IGame} from "./utils/types/types";
import {Stats} from "./components/Stats/Stats";
import {IRadioItem, Radiogroup} from "reactutils/src/components/Radiogroup/Radiogroup";
import Popup from "reactutils/src/components/Popup/Popup";
import Icon from "reactutils/src/components/Icon/Icon";

import './styles.scss';

const {useRef, useState} = React; 

export interface IState {
    number?: string;
}

export interface IProps {
    context?: any;
}

const SOURCE = {
    BGG: 1,
    TESERA: 2,
    // NASTOLIO: 3,
};
const DEFAULT_SOURCE = SOURCE.TESERA;

const COLLECTION_ALIASES = {
    OWN: COLLECTION_TYPE.OWN,
    PLAYED: COLLECTION_TYPE.PLAYED,
    RATED: COLLECTION_TYPE.RATED,
};
const DEFAULT_COLLECTION_TYPE = COLLECTION_TYPE.OWN;

const RadioItems_SOURCES = Object.keys(SOURCE).map(name => {
    return {
        label: name,
        value: SOURCE[name],
    } as IRadioItem;
});

const RadioItems_COLLECTION_TYPE = Object.keys(COLLECTION_ALIASES).map(name => {
    return {
        label: name,
        value: COLLECTION_ALIASES[name],
    } as IRadioItem;
});

export function App() {
    const defaultUsername: string = localStorage.username;
    const defaultSource: number = parseInt(localStorage.source, 10) || RadioItems_SOURCES[0].value;
    const defaultType: COLLECTION_TYPE = parseInt(localStorage.type, 10) || RadioItems_COLLECTION_TYPE[0].value;
    const sourceRef = useRef<HTMLSelectElement>(null);
    const userRef = useRef<HTMLInputElement>(null);
    const typeRef = useRef<HTMLSelectElement>(null);
    const [isValidForm, setIsValidForm] = useState(!!defaultUsername);
    const [games, setGames] = useState<void | IGame[]>(void 0);
    const [error, setError] = useState<string>('');
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [source, setSource] = useState<number>(defaultSource);
    const [type, setType] = useState<COLLECTION_TYPE>(defaultType);

    function getData() {
        setGames(void 0);
        setIsLoading(true);
        const user = userRef?.current?.value;
        
        if (!source || !user || !type) {
            return;
        }

        const data = {
            source,
            user,
            type,
        };

        if (source === SOURCE.BGG) {
            fetchBGG(user, type).then(games => {
                setError('');
                setGames(games);
                setIsLoading(false);
            });
        }
        else if (source === SOURCE.TESERA) {
            fetchTesera(user, type).then(games => {
                setError('');
                setGames(games);
                setIsLoading(false);
            }).catch(err => {
                setError(err.message);
                setIsLoading(false);
            });
        }
    }

    function checkValid(event) {
        const value = event.target.value;

        localStorage.username = value;

        const isHasValue = !!value;

        if (isHasValue !== isValidForm) {
            setIsValidForm(isHasValue);
        }
    }

    function onSourceChanged(newValue: number) {
        setSource((val) => newValue);
        localStorage.source = newValue;
    }

    function onTypeChanged(newValue: COLLECTION_TYPE) {
        setType((val) => newValue);
        localStorage.type = newValue;
    }

    function onClose() {
        setGames((val) => void 0);
        setError((val) => '');
    }

    return <div className='mainForm'>
            <Radiogroup 
                label={'Source'} 
                items={RadioItems_SOURCES} 
                onChange={onSourceChanged}
                defaultValue={defaultSource}
            />
            <label>
                <p>Username</p>
                <input 
                    type="text"
                    ref={userRef}
                    onChange={checkValid}
                    defaultValue={defaultUsername}
                />
            </label>
            <Radiogroup 
                label={'Type'} 
                items={RadioItems_COLLECTION_TYPE} 
                onChange={onTypeChanged}
                defaultValue={defaultType}
            />
            <button onClick={getData} disabled={!isValidForm || isLoading}>
                {isLoading ? <Icon name='spinner' className='preloader' /> : "Send"}
            </button>
            {error ? <Popup onClose={onClose}><p>{error}</p></Popup> : void 0}
            {(games && !error) ? <Popup onClose={onClose}><Stats games={games} /></Popup> : void 0}
        </div>
}

/*<Icon name='spinner' className='preloader' />*/

export default hot(module)(App);
